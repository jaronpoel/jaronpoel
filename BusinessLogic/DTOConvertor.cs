﻿using BusinessLogic;
using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class DTOConvertor
    {
        // voor User
        internal static UserDto GetUserDTO(User user)
        {
            UserDto dto = new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.Username,
                Password = user.Password
            };
            return dto;
        }

        internal static User GetUserFromDTO(UserDto dto)
        {
            return new User(dto.Id, dto.Username, dto.Password, dto.Email);
        }


        // voor Article
        internal static Article GetArticleFromDTO(ArticleDto dto)
        {
            return new Article(dto.Id, dto.Name);
        }

        internal static ArticleDto GetArticleDTO(Article article)
        {
            ArticleDto dto = new ArticleDto
            {
                Name = article.Name
            };
            return dto;
        }

        //Voor Ingredients
        internal static Ingredient GetIngredientFromDTO(IngredientDto dto)
        {
            return new Ingredient(dto.Id, dto.Number, dto.Measurement, dto.Article, dto.RecipeId);
        }


        //voor Shoppinlist
        internal static Shoppinglist GetShoppinglistFromDTO(ShoppinglistDto dto)
        {
            return new Shoppinglist(dto.Id, dto.Date, dto.Title, GetAllArticles(dto.Articles));
        }

        internal static ShoppinglistDto GetShoppinlistDTO(Shoppinglist shoppinglist)
        {
            ShoppinglistDto dto = new ShoppinglistDto
            {
                Id = shoppinglist.Id,
                Date = shoppinglist.Date,
                Title = shoppinglist.Title
            };
            return dto;
        }

        internal static List<Article> GetAllArticles(List<ArticleDto> list)
        {
            List<Article> articles = new List<Article>();
            foreach (ArticleDto articledto in list)
            {
                articles.Add(GetArticleFromDTO(articledto));
            }
            return articles;
        }

        internal static List<Shoppinglist> GetAllShoppinglists(List<ShoppinglistDto> list)
        {
            List<Shoppinglist> shoppinglist = new List<Shoppinglist>();
            foreach (ShoppinglistDto shoppinglistdto in list)
            {
                shoppinglist.Add(GetShoppinglistFromDTO(shoppinglistdto));
            }
            return shoppinglist;
        }


        //voor Recipe
        internal static Recipe GetRecipeFromDTO(RecipeDto dto)
        {
            return new Recipe(dto.Id, dto.Date, dto.Title, dto.Roadmap, GetAllIngredients(dto.Ingredients));
        }

        internal static RecipeDto GetRecipeDTO(Recipe recipe)
        {
            RecipeDto dto = new RecipeDto
            {
                Id = recipe.Id,
                Date = recipe.Date,
                Title = recipe.Title,
                Roadmap = recipe.Roadmap
            };
            return dto;
        }

        internal static List<Ingredient> GetAllIngredients(List<IngredientDto> list)
        {
            List<Ingredient> ingredients = new List<Ingredient>();
            foreach (IngredientDto ingredientdto in list)
            {
                ingredients.Add(GetIngredientFromDTO(ingredientdto));
            }
            return ingredients;
        }

        internal static List<Recipe> GetAllRecipes(List<RecipeDto> list)
        {
            List<Recipe> recipes = new List<Recipe>();
            foreach (RecipeDto recipedto in list)
            {
                recipes.Add(GetRecipeFromDTO(recipedto));
            }
            return recipes;
        }
    }
} 