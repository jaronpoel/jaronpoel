﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class Shoppinglist
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public List<Article> Articles{ get; set; }

        public Shoppinglist(int id, DateTime date, string title, List<Article> articles)
        {
            Id = id;
            Date = date;
            Title = title;
            Articles = articles;
        }

        private readonly IShoppinglistRepository shoppinglistRepository;

        public Shoppinglist(IShoppinglistContext context)
        {
            shoppinglistRepository = new ShoppinglistRepository(context);
        }
        public ShoppinglistDto SetTitle(string title)
        {
            throw new NotImplementedException();
        }

        public void AddArticle(int shoppinglistid, int articleid)
        {
            shoppinglistRepository.AddArticle(shoppinglistid, articleid);
        }

        
    }
}
