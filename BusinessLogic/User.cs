﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public User(int id, string username, string password, string email)
        {
            Id = id;
            Username = username;
            Password = password;
            Email = email;
        }

        private readonly IUserRepository userRepository;

        public User(IUserContext context)
        {
            userRepository = new UserRepository(context);
        }

        public void SetName(string name)
        {
            throw new NotImplementedException();
        }

        public void SetPassword(string password)
        {
            throw new NotImplementedException();
        }

        public void SetEmail(string email)
        {
            throw new NotImplementedException();
        }

        public ShoppinglistDto AddShoppinglist(Shoppinglist shoppinglist)
        {
            return userRepository.AddShoppinglist(DTOConvertor.GetShoppinlistDTO(shoppinglist));
        }
        public ShoppinglistDto DeleteShoppinglist(Shoppinglist shoppinglist)
        {
            throw new NotImplementedException();
        }

        public UserDto FollowUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
