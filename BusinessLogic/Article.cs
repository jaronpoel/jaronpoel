﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Article(int id, string name)
        {
            Id = id;
            Name = name;
        }

        private readonly IArticleRepository articleRepository;

        public Article(IArticleContext context)
        {
            articleRepository = new ArticleRepository(context);
        }
    }
}
