﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Collection
{
    public class RecipeCollectionLogic
    {
        private readonly IRecipeCollectionRepository recipeCollectionRepository;

        public RecipeCollectionLogic(IRecipeCollectionContext context)
        {
            recipeCollectionRepository = new RecipeCollectionRepository(context);
        }

        public void AddRecipe(Recipe recipe)
        {
            recipeCollectionRepository.AddRecipe(DTOConvertor.GetRecipeDTO(recipe));
        }

        public void DeleteRecipe(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        public List<Recipe> GetAllRecipes()
        {
            return DTOConvertor.GetAllRecipes(recipeCollectionRepository.GetAllRecipes());
        }

        public Recipe GetRecipeByID(int id)
        {
            return DTOConvertor.GetRecipeFromDTO(recipeCollectionRepository.GetRecipeByID(id));
        }
    }
}
