﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Collection
{
    public class ArticleCollectionLogic
    {
        private readonly IArticleCollectionRepository articleCollectionRepository;

        public ArticleCollectionLogic(IArticleCollectionContext context)
        {
            articleCollectionRepository = new ArticleCollectionRepository(context);
        }

        public void AddArticle(Article article)
        {
           articleCollectionRepository.AddArticle(DTOConvertor.GetArticleDTO(article));
        }

    }
}
