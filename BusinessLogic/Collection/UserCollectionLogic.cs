﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Collection
{
    public class UserCollectionLogic
    {
        private readonly IUserCollectionRepository userCollectionRepository;

        public UserCollectionLogic(IUserCollectionContext context)
        {
            userCollectionRepository = new UserCollectionRepository(context);
        }

        public void SignUp(string username, string password, string email)
        {
            throw new NotImplementedException();
        }

        public User SignIn(string username, string password)
        {
            return DTOConvertor.GetUserFromDTO(userCollectionRepository.SignIn(username, password));
        }
    }
}
