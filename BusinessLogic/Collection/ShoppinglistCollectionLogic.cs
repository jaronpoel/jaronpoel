﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Collection
{
    public class ShoppinglistCollectionLogic
        { 
        private readonly IShoppinglistCollectionRepository shoppinglistCollectionRepository;

        public ShoppinglistCollectionLogic(IShoppinglistCollectionContext context)
        {
            shoppinglistCollectionRepository = new ShoppinglistCollectionRepository(context);
        }

        public List<Shoppinglist> GetAllShoppinglists()
        {
            return DTOConvertor.GetAllShoppinglists(shoppinglistCollectionRepository.GetAllShoppinglists());
        }

        public Shoppinglist GetShoppinglistByID(int id)
        {
            return DTOConvertor.GetShoppinglistFromDTO(shoppinglistCollectionRepository.GetShoppinglistByID(id));
        }

        public List<Article> Search(string article)
        {
            return DTOConvertor.GetAllArticles(shoppinglistCollectionRepository.Search(article));
        }
    }
}
