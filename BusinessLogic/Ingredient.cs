﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class Ingredient
    {
        public int Id { get; }
        public int Number { get; }
        public string Measurement { get; }
        public string Article { get; }
        public int RecipeId { get; }

        public Ingredient(int id, int number, string measurment, string article, int recipeid)
        {
            Id = id;
            Number = number;
            Measurement = measurment;
            Article = article;
            RecipeId = recipeid;
        }

        private readonly IIngredientRepository ingredientRepository;

        public Ingredient(IIngredientContext context)
        {
            ingredientRepository = new IngredientRepository(context);
        }

        public void GetArticle()
        {
            throw new NotImplementedException();
        }
    }
}
