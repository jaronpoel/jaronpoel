﻿using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;

namespace BusinessLogic
{
    public class Recipe
    {
        public int Id { get; }
        public DateTime Date { get; }
        public string Title { get; }
        public string Roadmap { get; }
        public List<Ingredient> Ingredients { get; set; }

        public Recipe(int id, DateTime date, string title, string roadmap, List<Ingredient> ingredients)
        {
            Id = id;
            Date = date;
            Title = title;
            Roadmap = roadmap;
            Ingredients = ingredients;
        }

        private readonly IRecipeRepository recipeRepository;

        public Recipe(IRecipeContext context)
        {
            recipeRepository = new RecipeRepository(context);
        }

        public void AddIngredient(IngredientDto ingredient)
        {
            throw new NotImplementedException();
        }

        public void DeleteIngredient(IngredientDto ingredient)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecipe(RecipeDto recipe)
        {
            throw new NotImplementedException();
        }

        public List<RecipeDto> GetAllRecipes()
        {
            throw new NotImplementedException();
        }

        public RecipeDto GetRecipeByID(int id)
        {
            throw new NotImplementedException();
        }

        public void SetRoadmap(string roadmap)
        {
            throw new NotImplementedException();
        }

        public void SetTitle(string title)
        {
            throw new NotImplementedException();
        }
    }
}
