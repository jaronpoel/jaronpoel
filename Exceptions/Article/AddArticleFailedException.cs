﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Article
{
    public class AddArticleFailedException : Exception
    {
        public AddArticleFailedException()
        {

        }

        public AddArticleFailedException(string message) : base(message)
        {

        }
    }
}
