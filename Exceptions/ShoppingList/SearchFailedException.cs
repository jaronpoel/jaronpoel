﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Shoppinglist
{
    public class SearchFailedException : Exception
    {
        public SearchFailedException()
        {

        }

        public SearchFailedException(string message) : base(message)
        {

        }
    }
}
