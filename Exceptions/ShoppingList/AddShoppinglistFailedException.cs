﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Shoppinglist
{
    public class AddShoppinglistFailedException : Exception
    {
        public AddShoppinglistFailedException()
        {

        }

        public AddShoppinglistFailedException(string message) : base(message)
        {

        }
    }
}
