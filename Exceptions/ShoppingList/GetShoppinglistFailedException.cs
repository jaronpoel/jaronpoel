﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Shoppinglist
{
    public class GetShoppinglistFailedException : Exception
    {
        public GetShoppinglistFailedException()
        {

        }

        public GetShoppinglistFailedException(string message) : base(message)
        {

        }
    }
}
