﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Shoppinglist
{
    public class AddArticleToListFailedException : Exception
    {
        public AddArticleToListFailedException()
        {

        }

        public AddArticleToListFailedException(string message) : base(message)
        {

        }
    }
}
