﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Recipe
{
    public class AddRecipeFailedException : Exception
    {
        public AddRecipeFailedException()
        {

        }

        public AddRecipeFailedException(string message) : base(message)
        {

        }
    }
}
