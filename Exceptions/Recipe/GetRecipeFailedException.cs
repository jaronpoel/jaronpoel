﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Recipe
{
    public class GetRecipeFailedException : Exception
    {
        public GetRecipeFailedException()
        {

        }

        public GetRecipeFailedException(string message) : base(message)
        {

        }
    }
}
