﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions.Recipe
{
    public class AddIngriedentFailedException : Exception
    {
        public AddIngriedentFailedException()
        {

        }

        public AddIngriedentFailedException(string message) : base(message)
        {

        }
    }
}
