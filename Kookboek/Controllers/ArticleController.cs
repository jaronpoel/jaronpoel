﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Kookboek.Models;
using Interfaces.Contexts;
using Exceptions.Article;
using Interfaces.Dto;
using BusinessLogic.Collection;
using BusinessLogic;

namespace Kookboek.Controllers
{
    public class ArticleController : Controller
    {
        private readonly ArticleCollectionLogic articleCollectionLogic;

        public ArticleController(IArticleCollectionContext context)
        {
            articleCollectionLogic = new ArticleCollectionLogic(context);
        }
        public IActionResult NewArticle()
        {
            return View();
        }

        [HttpPost]
        public IActionResult New(NewArticleViewModel article)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("NewArticle", "Article");
            }

            try
            {
                Article newarticle = new Article(0, article.Name);

                articleCollectionLogic.AddArticle(newarticle);
                return RedirectToAction("NewArticle", "Article");
            }
            catch (AddArticleFailedException exception)
            {
                ModelState.AddModelError("", exception.Message);
                return RedirectToAction("NewArticle", "Article");
            }
        }
    }
}
