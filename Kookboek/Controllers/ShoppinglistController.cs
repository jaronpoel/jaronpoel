﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Kookboek.Models;
using Interfaces.Contexts;
using Microsoft.AspNetCore.Http;
using Interfaces.Dto;
using BusinessLogic.Collection;
using BusinessLogic;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Exceptions.Article;
using Exceptions.Shoppinglist;

namespace Kookboek.Controllers
{
    public class ShoppinglistController : Controller
    {
        private readonly ShoppinglistCollectionLogic ShoppinglistCollectionLogic;
        private readonly Shoppinglist ShoppinglistLogic;
        private readonly User UserLogic;

        public ShoppinglistController(IShoppinglistCollectionContext context, IShoppinglistContext _context, IUserContext usercontext)
        {
            ShoppinglistCollectionLogic = new ShoppinglistCollectionLogic(context);
            ShoppinglistLogic = new Shoppinglist(_context);
            UserLogic = new User(usercontext);
        }

        public IActionResult Shopping()
        {
            AllShoppinglistViewModel ViewModel = new AllShoppinglistViewModel();
            ViewModel.ListOfShoppinglists = ShoppinglistCollectionLogic.GetAllShoppinglists();
            return View(ViewModel);
        }

        public IActionResult NewShoppinglist()
        {
            return View();
        }

        public IActionResult Singlelist(int id)
        {
            Shoppinglist list = ShoppinglistCollectionLogic.GetShoppinglistByID(id);
            if (list == null)
            {
                return RedirectToAction("Shopping", "Shoppinglist");
            }
            ViewBag.Shoppinglist = list;
            ViewBag.Articles = new List<Article>();
            return View();
        }

        [HttpPost]
        public IActionResult NewList(ShoppinglistViewModel shoppinglist)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Shopping", "NewShoppinglist");
            }

            try
            {
                Shoppinglist newshoppinglist = new Shoppinglist(0,DateTime.Now, shoppinglist.Title, null);
                UserLogic.AddShoppinglist(newshoppinglist);
                return RedirectToAction("Index", "Home");
            }
            catch (AddShoppinglistFailedException exception)
            {
                ModelState.AddModelError("", exception.Message);
                return RedirectToAction("Shopping", "NewShoppinglist");
            }
        }

        [HttpPost]
        public IActionResult Singlelist(int id, SearchViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Shoppinglist list = ShoppinglistCollectionLogic.GetShoppinglistByID(id);
                if (list == null)
                {
                    return RedirectToAction("Shopping", "Shoppinglist");
                }
                ViewBag.Shoppinglist = list;
                ViewBag.Articles = new List<Article>();
                return View();
            }

            try
            {
                List<Article> articles = ShoppinglistCollectionLogic.Search(model.Query);
                if (!articles.Any())
                {
                    ModelState.AddModelError("", "No articles were found.");
                }

                Shoppinglist list = ShoppinglistCollectionLogic.GetShoppinglistByID(id);
                if (list == null)
                {
                    return RedirectToAction("Shopping", "Shoppinglist");
                }

                ViewBag.Shoppinglist = list;
                ViewBag.Articles = articles;
                return View();
            }
            catch (SearchFailedException)
            {
                Shoppinglist list = ShoppinglistCollectionLogic.GetShoppinglistByID(id);
                if (list == null)
                {
                    return RedirectToAction("Shopping", "Shoppinglist");
                }
                ViewBag.Shoppinglist = list;
                ViewBag.Articles = new List<Article>();
                ModelState.AddModelError("", "No articles were found.");
                return View();
            }
        }

        [HttpPost]
        public IActionResult AddArticleToShoppinlist(int shoppinglistid ,int articleid)
        {
            try
            {
                ShoppinglistLogic.AddArticle(shoppinglistid, articleid);
                return RedirectToAction("Singlelist", "Shoppinglist", new { id = shoppinglistid });
            }
            catch (AddShoppinglistFailedException exception)
            {
                ModelState.AddModelError("", exception.Message);
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
