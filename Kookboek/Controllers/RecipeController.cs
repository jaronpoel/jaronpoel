﻿using Kookboek.Models;
using Interfaces.Contexts;
using Logic.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BusinessLogic.Collection;
using BusinessLogic;
using System;
using Exceptions.Recipe;

namespace Kookboek.Controllers
{
    public class RecipeController : Controller
    {
        private readonly RecipeCollectionLogic RecipeLogic;
        public RecipeController(IRecipeCollectionContext context)
        {
            RecipeLogic = new RecipeCollectionLogic(context);
        }

        public IActionResult Book()
        {
            AllRecipesViewModel ViewModel = new AllRecipesViewModel();
            ViewModel.ListOfRecipes = RecipeLogic.GetAllRecipes();
            return View(ViewModel);
        }

        public IActionResult NewRecipe()
        {
            return View();
        }

        public IActionResult SingleRecipe(int id)
        {
            Recipe list = RecipeLogic.GetRecipeByID(id);
            if (list == null)
            {
                return RedirectToAction("Recipe", "Book");
            }
            ViewBag.Recipe = list;
            return View();
        }

        [HttpPost]
        public IActionResult NewRecipeAdded(RecipeViewModel recipe)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("NewRecipe", "Recipe");
            }

            try
            {
                Recipe newrecipe = new Recipe(0, DateTime.Now, recipe.Title, recipe.Roadmap, null);
                RecipeLogic.AddRecipe(newrecipe);
                return RedirectToAction("Index", "Home");
            }
            catch (AddRecipeFailedException exception)
            {
                ModelState.AddModelError("", exception.Message);
                return RedirectToAction("NewArticle", "Article");
            }
        }

        [HttpPost]
        public IActionResult AddIngreidentToRecipe(int shoppinglistid, int articleid)
        {
            try
            {
                return RedirectToAction("Singlelist", "Shoppinglist", new { id = shoppinglistid });
            }
            catch (AddIngriedentFailedException exception)
            {
                ModelState.AddModelError("", exception.Message);
                return RedirectToAction("Index", "Home");
            }
        }
    }
}