﻿using Kookboek.Models;
using Interfaces.Contexts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Exceptions.User;
using Interfaces.Dto;
using BusinessLogic.Collection;
using BusinessLogic;

namespace Kookboek.Controllers
{
    public class UserController : Controller
    {
        private readonly UserCollectionLogic UserCollectionLogic;
        public UserController(IUserCollectionContext context)
        {
            UserCollectionLogic = new UserCollectionLogic(context);
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }

        public ActionResult Info()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SignOut()
        {
            HttpContext.Session.SetString("username", string.Empty);
            HttpContext.Session.SetInt32("userid", 0);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SignIn(SignInViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                User auth = UserCollectionLogic.SignIn(user.Username, user.Password);
                HttpContext.Session.SetInt32("userid", auth.Id);
                HttpContext.Session.SetString("username", auth.Username);
                return RedirectToAction("Index", "Home");
            }
            catch (AuthenticationFailedException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost]
        public IActionResult SignUp(SignUpViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                UserCollectionLogic.SignUp(username: user.Username, password: user.Password, email: user.Email);
                return SignIn();
            }
            catch (SignUpFailedException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
    }
}