﻿using System.ComponentModel.DataAnnotations;

namespace Kookboek.Models
{
    public class SignInViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
