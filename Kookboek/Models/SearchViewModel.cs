﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kookboek.Models
{
    public class SearchViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [StringLength(30)]
        public string Query { get; set; }
    }
}
