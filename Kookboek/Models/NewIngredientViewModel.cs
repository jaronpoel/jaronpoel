﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kookboek.Models
{
    public class NewIngredientViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        public int Number { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Measurement { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Article { get; set; }
    }
}
