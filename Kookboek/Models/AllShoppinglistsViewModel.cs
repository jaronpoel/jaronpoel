﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kookboek.Models
{
    public class AllShoppinglistViewModel
    {
        public List<Shoppinglist> ListOfShoppinglists { get; set; }
    }
}
