﻿using BusinessLogic;
using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kookboek.Models
{
    public class AllRecipesViewModel
    {
        public List<Recipe> ListOfRecipes { get; set; }
    }
}
