﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kookboek.Models
{
    public class ShoppinglistViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string Title { get; set; }
    }
}
