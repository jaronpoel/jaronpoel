﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Dto
{
    public class ArticleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
