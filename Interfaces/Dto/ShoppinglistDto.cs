﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Dto
{
    public class ShoppinglistDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public List<ArticleDto> Articles { get; set; }
    }
}
