﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Dto
{
    public class IngredientDto
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Measurement { get; set; }
        public string Article { get; set; }
        public int RecipeId { get; set; }
    }
}
