﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Dto
{
    public class RecipeDto
    {
        public int Id;

        public DateTime Date;

        public string Title;

        public List<IngredientDto> Ingredients;

        public string Roadmap;
    }
}
