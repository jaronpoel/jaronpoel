﻿using Interfaces.Dto;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Repositories
{
    public interface IUserRepository
    {
        void SetName(string name);
        void SetPassword(string password);
        void SetEmail(string email);
        ShoppinglistDto AddShoppinglist(ShoppinglistDto shoppinglist);
        ShoppinglistDto DeleteShoppinglist(ShoppinglistDto shoppinglist);
        UserDto FollowUser(UserDto user);
    }
}
