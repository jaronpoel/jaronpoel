﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Dto;
using Models.Data;

namespace Interfaces.Repositories
{
    public interface IRecipeRepository
    {
        void SetTitle(string title);
        void SetRoadmap(string roadmap);
    }
}
