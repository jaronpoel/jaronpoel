﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Dto;
using Models.Data;

namespace Interfaces.Repositories
{
    public interface IShoppinglistRepository
    {
        ShoppinglistDto SetTitle(string title);
        void AddArticle(int Shoppinlistid, int articleid);
    }
}
