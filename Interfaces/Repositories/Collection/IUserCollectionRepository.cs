﻿using Interfaces.Dto;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Repositories
{
    public interface IUserCollectionRepository
    {
        UserDto SignIn(string username, string password);
        void SignUp(string username, string password, string email);
    }
}
