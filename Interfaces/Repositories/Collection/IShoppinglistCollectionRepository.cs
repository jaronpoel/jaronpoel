﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Dto;
using Models.Data;

namespace Interfaces.Repositories
{
    public interface IShoppinglistCollectionRepository
    {
        List<ShoppinglistDto> GetAllShoppinglists();
        ShoppinglistDto GetShoppinglistByID(int id);
        List<ArticleDto> Search(string article);
    }
}
