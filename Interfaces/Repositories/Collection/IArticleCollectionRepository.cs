﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Repositories
{
    public interface IArticleCollectionRepository
    {
        void AddArticle(ArticleDto article);
    }
}
