﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Dto;
using Models.Data;

namespace Interfaces.Repositories
{
    public interface IRecipeCollectionRepository
    {
        List<RecipeDto> GetAllRecipes();
        void AddRecipe(RecipeDto recipe);
        void DeleteRecipe(RecipeDto recipe);
        RecipeDto GetRecipeByID(int id);
    }
}
