﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IRecipeContext
    {
        void SetTitle(string title);
        void SetRoadmap(string roadmap);
    }
}
