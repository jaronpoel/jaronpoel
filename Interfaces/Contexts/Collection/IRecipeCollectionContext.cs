﻿using System;
using Interfaces.Dto;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IRecipeCollectionContext
    {
        List<RecipeDto> GetAllRecipes();
        void AddRecipe(RecipeDto recipe);
        void DeleteRecipe(RecipeDto recipe);
        RecipeDto GetRecipeByID(int id);
    }
}
