﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IUserCollectionContext
    {
        UserDto SignIn(string username, string password);
        void SignUp(string username, string password, string email);
    }
}
