﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IArticleCollectionContext
    {
        void AddArticle(ArticleDto article);
    }
}
