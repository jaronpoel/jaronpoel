﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IShoppinglistCollectionContext
    {
        List<ShoppinglistDto> GetAllShoppinglists();
        ShoppinglistDto GetShoppinglistByID(int id);
        List<ArticleDto> Search(string article);
    }
}
