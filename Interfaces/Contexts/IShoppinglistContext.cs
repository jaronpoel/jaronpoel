﻿using Interfaces.Dto;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IShoppinglistContext
    {
        ShoppinglistDto SetTitle(string title);
        void AddArticle(int shoppinglistid, int articleid);
    }
}
