﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IUserContext
    {
        void SetName(string name);
        void SetPassword(string password);
        void SetEmail(string email);
        ShoppinglistDto AddShoppinglist(ShoppinglistDto shoppinglist);
        ShoppinglistDto DeleteShoppinglist(ShoppinglistDto shoppinglist);
        UserDto FollowUser(UserDto user);
    }
}
