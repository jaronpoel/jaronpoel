﻿using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public interface IIngredientContext
    {
        ArticleDto GetArticle();
    }
}
