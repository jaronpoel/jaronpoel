﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Data
{
    public class Recipe
    {
        public int Id;
        public DateTime Date;
        public string Title;
        public string Ingredients;
        public string Roadmap;
    }
}
