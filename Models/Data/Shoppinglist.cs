﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Data
{
    public class Shoppinglist
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string MadeBy { get; set; }
        public string Article { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
