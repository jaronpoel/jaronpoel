﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Data
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
