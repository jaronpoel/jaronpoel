﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Data
{
    public class Ingredient
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Measurement { get; set; }
        public string Article { get; set; }
    }
}
