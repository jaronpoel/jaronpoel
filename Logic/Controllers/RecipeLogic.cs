﻿using System;
using System.Collections.Generic;
using System.Text;
using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Logic;
using Interfaces.Repositories;
using Models.Data;

namespace Logic.Controllers
{
    public class RecipeLogic : IRecipeLogic
    {
        private readonly IRecipeRepository RecipeRepository;

        public RecipeLogic(IRecipeContext context)
        {
            RecipeRepository = new RecipeRepository(context);
        }

        public Recipe GetRecipeByID(int id)
        {
            throw new NotImplementedException();
        }

        public void SetTitle(string title)
        {
            throw new NotImplementedException();
        }

        public void SetRoadmap(string roadmap)
        {
            throw new NotImplementedException();
        }

        public void AddIngredient(IngredientDto ingredient)
        {
            throw new NotImplementedException();
        }

        public void DeleteIngredient(IngredientDto ingredient)
        {
            throw new NotImplementedException();
        }
    }
}
