﻿using System;
using System.Collections.Generic;
using System.Text;
using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Logic;
using Interfaces.Repositories;
using Models.Data;


namespace Logic.Controllers
{
    public class UserLogic : IUserLogic
    {
        private readonly IUserRepository UserRepository;

        public UserLogic(IUserContext context)
        {
            UserRepository = new UserRepository(context);
        }

        public void SetName(string name)
        {
            throw new NotImplementedException();
        }

        public void SetPassword(string password)
        {
            throw new NotImplementedException();
        }

        public void SetEmail(string email)
        {
            throw new NotImplementedException();
        }

        public User FollowUser(User user)
        {
            throw new NotImplementedException();
        }

        public UserDto FollowUser(UserDto user)
        {
            throw new NotImplementedException();
        }
    }
}
