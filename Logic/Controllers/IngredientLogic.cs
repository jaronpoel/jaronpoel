﻿using System;
using System.Collections.Generic;
using System.Text;
using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Logic;
using Interfaces.Repositories;
using Models.Data;

namespace Logic.Controllers
{
    public class IngredientLogic : IIngredientLogic
    {
        private readonly IIngredientRepository IngredientRepository;

        public IngredientLogic(IIngredientContext context)
        {
            IngredientRepository = new IngredientRepository(context);
        }
    }
}
