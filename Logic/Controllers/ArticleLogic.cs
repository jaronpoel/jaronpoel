﻿using System;
using System.Collections.Generic;
using System.Text;
using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Logic;
using Interfaces.Repositories;
using Models.Data;

namespace Logic.Controllers
{
    public class ArticleLogic : IArticleLogic
    {
        private readonly IArticleRepository ArticleRepository;

        public ArticleLogic(IArticleContext context)
        {
            ArticleRepository = new ArticleRepository(context);
        }
    }
}
