﻿using System;
using System.Collections.Generic;
using System.Text;
using Dal.Repositories;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Logic;
using Interfaces.Repositories;
using Models.Data;

namespace Logic.Controllers
{
    public class ShoppinglistLogic : IShoppinglistLogic
    {
        private readonly IShoppinglistRepository ShoppinglistRepository;

        public ShoppinglistLogic(IShoppinglistContext context)
        {
            ShoppinglistRepository = new ShoppinglistRepository(context);
        }

        public ArticleDto AddArticle(ArticleDto name)
        {
            throw new NotImplementedException();
        }

        public ShoppinglistDto SetTitle(string title)
        {
            throw new NotImplementedException();
        }
    }
}
