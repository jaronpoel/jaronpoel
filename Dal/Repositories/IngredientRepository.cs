﻿using Interfaces.Contexts;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dal.Repositories
{
    public class IngredientRepository : IIngredientRepository
    {
        private IIngredientContext Context;

        public IngredientRepository(IIngredientContext context)
        {
            Context = context;
        }
    }
}
