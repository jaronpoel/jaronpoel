﻿using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dal.Repositories
{
    public class ShoppinglistRepository : IShoppinglistRepository
    {
        private IShoppinglistContext Context;

        public ShoppinglistRepository(IShoppinglistContext context)
        {
            Context = context;
        }

        public void AddArticle(int shoppinglistid, int articleid)
        {
            Context.AddArticle(shoppinglistid, articleid);
        }

        public ShoppinglistDto SetTitle(string title)
        {
            throw new NotImplementedException();
        }
        
    }
}
