﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Models.Data;



namespace Dal.Repositories
{
    public class UserRepository : IUserRepository
    {
        private IUserContext Context;

        public UserRepository(IUserContext context)
        {
            Context = context;
        }

        public ShoppinglistDto AddShoppinglist(ShoppinglistDto shoppinglist)
        {
            return Context.AddShoppinglist(shoppinglist);
        }

        public ShoppinglistDto DeleteShoppinglist(ShoppinglistDto shoppinglist)
        {
            throw new NotImplementedException();
        }

        public UserDto FollowUser(UserDto user)
        {
            throw new NotImplementedException();
        }

        public void SetEmail(string email)
        {
            throw new NotImplementedException();
        }

        public void SetName(string name)
        {
            throw new NotImplementedException();
        }

        public void SetPassword(string password)
        {
            throw new NotImplementedException();
        }
    }
}
