﻿using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dal.Repositories
{
    public class ShoppinglistCollectionRepository : IShoppinglistCollectionRepository
    {
        private IShoppinglistCollectionContext Context;

        public ShoppinglistCollectionRepository(IShoppinglistCollectionContext context)
        {
            Context = context;
        }

        public List<ShoppinglistDto> GetAllShoppinglists()
        {
            return Context.GetAllShoppinglists();
        }

        public ShoppinglistDto GetShoppinglistByID(int id)
        {
            return Context.GetShoppinglistByID(id);
        }

        public List<ArticleDto> Search(string article)
        {
            return Context.Search(article);
        }
    }
}
