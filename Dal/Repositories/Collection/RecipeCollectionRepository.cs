﻿ using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dal.Repositories
{
    public class RecipeCollectionRepository : IRecipeCollectionRepository
    {
        private IRecipeCollectionContext Context;

        public RecipeCollectionRepository(IRecipeCollectionContext context)
        {
            Context = context;
        }

        public void AddRecipe(RecipeDto recipe)
        {
            Context.AddRecipe(recipe);
        }

        public void DeleteRecipe(RecipeDto recipe)
        {
            throw new NotImplementedException();
        }

        public List<RecipeDto> GetAllRecipes()
        {
            return Context.GetAllRecipes();
        }

        public RecipeDto GetRecipeByID(int id)
        {
            return Context.GetRecipeByID(id);
        }
    }
}
