﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Models.Data;



namespace Dal.Repositories
{
    public class UserCollectionRepository : IUserCollectionRepository
    {
        private IUserCollectionContext Context;

        public UserCollectionRepository(IUserCollectionContext context)
        {
            Context = context;
        }

        public UserDto SignIn(string username, string password)
        {
            return Context.SignIn(username, password);
        }

        public void SignUp(string username, string password, string email)
        {
            Context.SignUp(username, password, email);
        }
    }
}
