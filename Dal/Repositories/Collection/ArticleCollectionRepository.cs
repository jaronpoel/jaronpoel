﻿using Interfaces.Contexts;
using Interfaces.Dto;
using Interfaces.Repositories;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dal.Repositories
{
    public class ArticleCollectionRepository : IArticleCollectionRepository
    {
        private IArticleCollectionContext Context;

        public ArticleCollectionRepository(IArticleCollectionContext context)
        {
            Context = context;
        }

        public void AddArticle(ArticleDto article)
        {
            Context.AddArticle(article);
        }
    }
}
