﻿using Exceptions.Shoppinglist;
using Exceptions.User;
using Interfaces.Contexts;
using Interfaces.Dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Dal.Contexts
{
    public class UserSqlContext : IUserContext
    {
        public ShoppinglistDto AddShoppinglist(ShoppinglistDto shoppinglist)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO [Shoppinglist] (Title ,UserId ,Date) VALUES (@Title ,@UserId ,@Date)";
                    command.Parameters.AddWithValue("@Title", shoppinglist.Title);
                    command.Parameters.AddWithValue("@UserId", 1);
                    command.Parameters.AddWithValue("@Date", shoppinglist.Date);
                    command.ExecuteNonQuery();

                    return shoppinglist;
                }
            }
            catch (SqlException)
            {
                throw new AddShoppinglistFailedException("An unexpected error occured.");
            }
        }

        public ShoppinglistDto DeleteShoppinglist(ShoppinglistDto shoppinglist)
        {
            throw new NotImplementedException();
        }

        public UserDto FollowUser(UserDto user)
        {
            throw new NotImplementedException();
        }

        public void SetEmail(string email)
        {
            throw new NotImplementedException();
        }

        public void SetName(string name)
        {
            throw new NotImplementedException();
        }

        public void SetPassword(string password)
        {
            throw new NotImplementedException();
        }
    }
}
