﻿using Exceptions.Shoppinglist;
using Interfaces.Contexts;
using Interfaces.Dto;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dal.Contexts
{
    public class ShoppinglistCollectionSqlContext : IShoppinglistCollectionContext
    {
        public ShoppinglistDto AddShoppinglist(ShoppinglistDto shoppinglist)
        {
            throw new NotImplementedException();
        }

        public ShoppinglistDto DeleteShoppinglist(ShoppinglistDto shoppinglist)
        {
            throw new NotImplementedException();
        }

        public ShoppinglistDto SetTitle(string title)
        {
            throw new NotImplementedException();
        }

        List<ShoppinglistDto> IShoppinglistCollectionContext.GetAllShoppinglists()
        {
            try
            {
                using (SqlConnection conn = DataConnection.GetConnection())
                {
                    conn.Open();
                    string query = "Select * FROM Shoppinglist Where userId = 1";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    List<ShoppinglistDto> AllShoppinglists = new List<ShoppinglistDto>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ShoppinglistDto shoppinglist = new ShoppinglistDto();

                        int.TryParse(dr[0].ToString(), out int id);
                        shoppinglist.Id = id;
                        int.TryParse(dr[1].ToString(), out int Userid);
                        shoppinglist.Title = dr[2].ToString();
                        shoppinglist.Date = (DateTime)dr[3];
                        shoppinglist.Articles = GetArticlesByShoppinglistId(shoppinglist.Id);

                        AllShoppinglists.Add(shoppinglist);
                    }
                    return (AllShoppinglists);
                }
            }
            catch (SqlException)
            {
                throw new GetShoppinglistFailedException("An unexpected error occured.");
            }
        }

        ShoppinglistDto IShoppinglistCollectionContext.GetShoppinglistByID(int id)
        {
            try
            {
                using (SqlConnection conn = DataConnection.GetConnection())
                {
                    conn.Open();
                    string query = "Select * From Shoppinglist WHERE Id=@id";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                    ShoppinglistDto shoppinglist = new ShoppinglistDto();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            shoppinglist.Id = (int)reader["Id"];
                            shoppinglist.Title = (string)reader["Title"];
                            shoppinglist.Date = (DateTime)reader["Date"];
                            shoppinglist.Articles = GetArticlesByShoppinglistId(shoppinglist.Id);
                        }
                    }
                    return (shoppinglist);
                }
            }
            catch (SqlException)
            {
                throw new GetShoppinglistFailedException("An unexpected error occured.");
            }
        }

        public List<ArticleDto> GetArticlesByShoppinglistId(int id)
        {
            try
            {
                using (SqlConnection conn = DataConnection.GetConnection())
                {
                    conn.Open();
                    string query = "SELECT * FROM Article INNER JOIN Shoppinglist_Article ON Shoppinglist_Article.ArticleId = Article.Id WHERE Shoppinglist_Article.ShoppinglistId=@ShoppinglistId";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@ShoppinglistId", id);
                    cmd.ExecuteNonQuery();
                    List<ArticleDto> articlelist = new List<ArticleDto>();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ArticleDto article = new ArticleDto
                            {
                                Name = (string)reader["Name"],
                                Id = (int)reader["Id"]
                            };
                            articlelist.Add(article);
                        }
                    }
                    return (articlelist);
                }
            }
            catch (SqlException)
            {
                throw new GetShoppinglistFailedException("An unexpected error occured.");
            }
        }

        public List<ArticleDto> Search(string article)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand($"SELECT * FROM Article WHERE Name LIKE '{article}%'", connection);
                    command.Parameters.AddWithValue("@article", article);
                    SqlDataReader reader = command.ExecuteReader();
                    List<ArticleDto> articles = new List<ArticleDto>();
                    while (reader.Read())
                    {
                        articles.Add(new ArticleDto()
                        {
                            Id = (int)reader["ID"],
                            Name = (string)reader["Name"],
                        });
                    }
                    return articles;
                }
            }
            catch (SqlException)
            {
                throw new SearchFailedException("An unexpected error occured.");
            }
        }
    }
}
