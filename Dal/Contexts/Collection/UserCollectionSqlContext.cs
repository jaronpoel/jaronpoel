﻿using Exceptions.User;
using Interfaces.Contexts;
using Interfaces.Dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Dal.Contexts
{
    public class UserCollectionSqlContext : IUserCollectionContext
    {
        public UserDto SignIn(string username, string password)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM Users WHERE Username = @Username AND Password = @Password", connection);
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@Password", password);

                    if ((int)command.ExecuteScalar() == 1)
                    {
                        SqlCommand scommand = new SqlCommand("SELECT ID, Username FROM Users WHERE Username = @Username", connection);
                        scommand.Parameters.AddWithValue("@Username", username);
                        SqlDataReader reader = scommand.ExecuteReader();
                        while (reader.Read())
                        {
                            return new UserDto()
                            {
                                Id = (int)reader["ID"],
                                Username = (string)reader["Username"],
                            };
                        }
                    }
                    throw new AuthenticationFailedException("Incorrect username or password.");
                }
            }
            catch (SqlException)
            {
                throw new AuthenticationFailedException("An unexpected error occured.");
            }
        }

        public void SignUp(string username, string password, string email)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO [Users] (Email, Username, Password) VALUES (@Email, @Username, @Password)";
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@Password", password);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException)
            {
                throw new SignUpFailedException("An unexpected error occured.");
            }
        }
    }
}
