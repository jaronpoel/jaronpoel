﻿using Exceptions.Recipe;
using Interfaces.Contexts;
using Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dal.Contexts
{
    public class RecipeCollectionSqlContext : IRecipeCollectionContext
    {
        public void AddRecipe(RecipeDto recipe)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO [Recipe] (Title ,UserId ,Date, Roadmap) VALUES (@Title ,@UserId ,@Date, @Roadmap)";
                    command.Parameters.AddWithValue("@Title", recipe.Title);
                    command.Parameters.AddWithValue("@UserId", 1);
                    command.Parameters.AddWithValue("@Date", recipe.Date);
                    command.Parameters.AddWithValue("@Roadmap", recipe.Roadmap);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException)
            {
                throw new AddRecipeFailedException("An unexpected error occured.");
            }
        }

        public void DeleteRecipe(RecipeDto recipe)
        {
            throw new NotImplementedException();
        }

        public List<RecipeDto> GetAllRecipes()
        {
            try
            {
                using (SqlConnection conn = DataConnection.GetConnection())
                {
                    conn.Open();
                    string query = "Select * FROM Recipe Where userId = 1";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    List<RecipeDto> AllRecipes = new List<RecipeDto>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        RecipeDto recipes = new RecipeDto();

                        int.TryParse(dr[0].ToString(), out int id);
                        recipes.Id = id;
                        int.TryParse(dr[1].ToString(), out int Userid);
                        recipes.Title = dr[2].ToString();
                        recipes.Roadmap = dr[3].ToString();
                        recipes.Date = (DateTime)dr[4];
                        recipes.Ingredients = GetIngrdientsByRecipeId(recipes.Id);

                        AllRecipes.Add(recipes);
                    }
                    return (AllRecipes);
                }
            }
            catch (SqlException)
            {
                throw new GetRecipeFailedException("An unexpected error occured.");
            }
        }

        public RecipeDto GetRecipeByID(int id)
        {
            try
            {
                using (SqlConnection conn = DataConnection.GetConnection())
                {
                    conn.Open();
                    string query = "Select * From Recipe WHERE Id=@id";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                    RecipeDto recipe = new RecipeDto();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            recipe.Id = (int)reader["Id"];
                            recipe.Title = (string)reader["Title"];
                            recipe.Date = (DateTime)reader["Date"];
                            recipe.Roadmap = (string)reader["Roadmap"];
                            recipe.Ingredients = GetIngrdientsByRecipeId(recipe.Id);
                        }
                    }
                    return (recipe);
                }
            }
            catch (SqlException)
            {
                throw new GetRecipeFailedException("An unexpected error occured.");
            }
        }

        public List<IngredientDto> GetIngrdientsByRecipeId(int id)
        {
            try
            {
                using (SqlConnection conn = DataConnection.GetConnection())
                {
                    conn.Open();
                    string query = "SELECT * FROM Ingredient WHERE Ingredient.RecipeId=@RecipeId";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@RecipeId", id);
                    cmd.ExecuteNonQuery();
                    List<IngredientDto> ingredientlist = new List<IngredientDto>();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            IngredientDto ingredient = new IngredientDto
                            {
                                Id = (int)reader["Id"],
                                Number = (int)reader["Number"],
                                Measurement = (string)reader["Measurement"],
                                Article = (string)reader["Article"],
                                RecipeId = (int)reader["RecipeId"]
                            };
                            ingredientlist.Add(ingredient);
                        }
                    }
                    return (ingredientlist);
                }
            }
            catch (SqlException)
            {
                throw new GetRecipeFailedException("An unexpected error occured.");
            }
        }
    }
}
