﻿using Exceptions.Article;
using Interfaces.Contexts;
using Models.Data;
using Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Interfaces.Dto;
using Exceptions.Shoppinglist;

namespace Dal.Contexts
{
    public class ArticleCollectionSqlContext : IArticleCollectionContext
    {
        public void AddArticle(ArticleDto article)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO [Article] (Name) VALUES (@Name)";
                    command.Parameters.AddWithValue("@Name", article.Name);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException)
            {
                throw new AddArticleFailedException("An unexpected error occured.");
            }
        }
    }
}
