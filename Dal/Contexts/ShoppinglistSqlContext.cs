﻿using Exceptions.Article;
using Exceptions.Shoppinglist;
using Interfaces.Contexts;
using Interfaces.Dto;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dal.Contexts
{
    public class ShoppinglistSqlContext : IShoppinglistContext
    {
        public void AddArticle(int shoppinlistid, int articleid)
        {
            try
            {
                using (SqlConnection connection = DataConnection.GetConnection())
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO [Shoppinglist_Article] (ShoppinglistId, ArticleId) VALUES (@ShoppinglistId, @ArticleId)";
                    command.Parameters.AddWithValue("@ShoppinglistId", shoppinlistid);
                    command.Parameters.AddWithValue("@ArticleId", articleid);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException)
            {
                throw new AddArticleFailedException("An unexpected error occured.");
            }
        }

        public ShoppinglistDto SetTitle(string title)
        {
            throw new NotImplementedException();
        }
    }
}
