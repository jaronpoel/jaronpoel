﻿using System.Data.SqlClient;

namespace Dal
{
    public static class DataConnection
    {
        private static readonly string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Jaron\Desktop\School\Project\Kookboek\Dal\Kookboek.mdf";

        public static SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}